import React, { useState } from 'react'
import { useAppDispatch } from '../Redux/hooks';
import { addItem, Items } from './Item.slice';
import "bulma/css/bulma.css";

const ItemForm: React.FC = () => {

  const dispatch = useAppDispatch()

  const [item, setItem] = useState<Items>({
    id: 0,
    title: '',
    price: 0,
    quantity: 0
  })

  const handleChange = ({ target: { name, value } }: React.ChangeEvent<HTMLInputElement>) => setItem(prev => {
    // prev[name] = value;
    // (prev as any)[name] = value;
    // let newValue = JSON.parse(JSON.stringify(prev));
    const newValue = {...prev, [name]:value  } ;
    return newValue;
  })

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    console.log({ item })
    dispatch(addItem(item))
  }

  return (<>
    <div className="box control">
      <label className="label is-large is-size-3" >Add Items</label>
      <form onSubmit={handleSubmit}>
        <input className="input is-medium" type="number" placeholder="id" name="id" defaultValue={item.id} onChange={handleChange}/>
        <input className="input is-medium" type="text" placeholder="item title" name="title" value={item.title} onChange={handleChange} />
        <input className="input is-medium" type="number" placeholder="price" name="price" value={item.price} onChange={handleChange} />
        <br />
        <input className="input is-medium" type="number" placeholder="quantity" name="quantity" value={item.quantity} onChange={handleChange} />
        <br />
        <br />
        <button type="submit" className="button is-large is-danger"> Add Item </button>
      </form>
    </div>
  </>);
}

// let id = 0;
// const createRandomItem = () => {
//   id = id + 1;
//   return {
//     id,
//     title: `Item number: ${id}`,
//     price: Number((Math.random() * 10 + 1).toFixed(2)),
//     quantity: Number((Math.random() * 10 + 1).toFixed(0))
//   };
// };

export default ItemForm