/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { useAppDispatch, useAppSelector } from '../Redux/hooks';
import { getItemsSelector, getTotalPrice, removeItem } from './Item.slice';
// import "bulma/css/bulma.css";
import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

interface Tip {
    id: number;
    value: number;
    prefix: string;
}

const Tips: Tip[] = [
    { id: 1, value: 0.10, prefix: "10" },
    { id: 2, value: 0.25, prefix: "25" }
];

const ItemMenu: React.FC = () => {

    const items = useSelector(getItemsSelector)
    const totalPrice = useAppSelector(getTotalPrice)
    let tipValue: number = 0;

    const [selectedTip, setSelectedTip] = useState(0);

    const theChosenFruit = () => {
        const chosenTip: Tip | undefined = Tips.find(
            f => f.id === selectedTip,
        );
        if (chosenTip) {
            tipValue = Math.round(totalPrice * chosenTip.value);
        }
        return chosenTip
            ? chosenTip.prefix
            : 'Select a Tip';
    };


    const dispatch = useAppDispatch()

    const removeFromStore = (id: number) => {
        dispatch(removeItem(id))
    }

    return (
        <div>
            <div className="table-container">
                <table className="table is-fullwidth has-text-left ">
                    <thead className="is-size-4 is-fullwidth">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    {items.map((item: any) =>
                        <tbody className="is-size-4 is-fullwidth" key={item.id} >
                            <tr>
                                <td>{`${item.id}`}</td>
                                <td>{`${item.title}`}</td>
                                <td>{`${item.quantity}`}</td>
                                <td>{`${item.price}`}</td>
                                <td>
                                    <button className="button is-small is-danger" onClick={() => removeFromStore(item.id)}>Remove</button>
                                </td>
                            </tr>
                        </tbody>
                    )}
                    <tfoot className="is-size-4 is-fullwidth">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tip Percentage: </td>
                            <td>
                                <div className="dropdown">
                                    <Dropdown onSelect={(e: string | null) => setSelectedTip(e ? Number(e) : 0)}>
                                        <Dropdown.Toggle>
                                            {theChosenFruit()}
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            {Tips.map(tip => {
                                                return (
                                                    <Dropdown.Item key={tip.id} eventKey={tip.id.toString()}>
                                                        {tip.prefix}
                                                    </Dropdown.Item>
                                                );
                                            })}
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Subtotal: </td>
                            <td>${totalPrice.toFixed(2)}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Tip: </td>
                            <td>${tipValue.toFixed(2)}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total: </td>
                            <td>${(totalPrice-tipValue).toFixed(2)}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    );
}

export default ItemMenu

