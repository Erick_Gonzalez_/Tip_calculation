import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../Redux/store';

export interface Items {
    title: string;
    price: number;
    quantity: number;
    id: number;
}

const initialState: Items[] = [
    {title: "Cyberpunk 2077", price: 60.98, quantity: 1, id: 1},
    {title: "Escape from Tarkov ", price: 30.25, quantity: 2, id: 2},
    {title: "Titanfall 2", price: 45.32, quantity: 3, id: 3}
]

const itemSlice = createSlice ({
    name: "items",
    initialState,
    reducers: {
        addItem: (state, action: PayloadAction<Items>) => {
            return [action.payload, ...state]
            // state.push(action.payload)
        },
        removeItem: (state, action: PayloadAction<number>) => {
            return state.filter(item => item.id !== action.payload)
        }
    }
});

export const {addItem, removeItem} = itemSlice.actions;

export const getItemsSelector = (state: RootState) => state.Items;
export const getTotalPrice = (state: RootState) => state.Items.reduce((acc, next) => acc += (next.price * next.quantity), 0)

export default itemSlice.reducer;
