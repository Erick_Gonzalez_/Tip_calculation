import { configureStore} from '@reduxjs/toolkit';
// import counterReducer from '../../features/counter/counterSlice';
import Items from '../Components/Item.slice';

export const store = configureStore({
  reducer: {
    // counter: counterReducer,
    Items
  },
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;

export default store;
// export type AppThunk<ReturnType = void> = ThunkAction<
//   ReturnType,
//   RootState,
//   unknown,
//   Action<string>
// >;
