import React from 'react';
// import { Counter } from './features/counter/Counter';
import './App.css';
import ItemMenu from './Components/ItemMenu';
import ItemForm  from './Components/ItemForm';
import { Provider } from 'react-redux';
import store from './Redux/store';
import "bulma/css/bulma.css";


function App() {
  return (
    <Provider store= {store}>
    <div className="App">
      <header className="App-header">
        {/* <Counter /> */}
        <ItemForm />
        <ItemMenu />
      </header>
    </div>
    </Provider>
  );
}

export default App;
